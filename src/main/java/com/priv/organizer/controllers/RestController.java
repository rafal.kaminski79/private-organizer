package com.priv.organizer.controllers;

import org.springframework.web.bind.annotation.GetMapping;


@org.springframework.web.bind.annotation.RestController
public class RestController {

	private static long countClicks =0;

	@GetMapping(value = "/increment")
	public long incrementClicks() {
		return countClicks++;
	}
}
