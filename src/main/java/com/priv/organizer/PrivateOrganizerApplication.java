package com.priv.organizer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrivateOrganizerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrivateOrganizerApplication.class, args);
	}

}
